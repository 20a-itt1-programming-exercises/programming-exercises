Exercise 1: What is the function of the secondary memory in a computer?
a) Execute all of the computation and logic of the program

Exercise 2: What is a program?
A computer program is a collection of instructions that can be executed by a computer to perform a specific task.

Exercise 3: What is the difference between a compiler and an interpreter?
Compiler transforms code written in a high-level programming language into the machine code, at once, before program runs, whereas an Interpreter coverts each high-level program statement, one by one, into the machine code, during program run. Compiled code runs faster while interpreted code runs slower.

Exercise 4: Which of the following contains “machine code”?
a) The Python interpreter

Exercise 5: What is wrong with the following code:
1.14. EXERCISES 17
>>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'
^
SyntaxError: invalid syntax

print spelled as primt. syntax 

Exercise 6: Where in the computer is a variable such as “x” stored after
the following Python line finishes?
x = 123
b) Main Memory

Exercise 7: What will the following program print out:
x = 43
x = x + 1
print(x)
b) 44

Exercise 8: Explain each of the following using an example of a human capability: 
(1) Central processing unit, 
 is the part of the computer that is
built to be obsessed with “what is next?” If your computer is rated at 3.0
Gigahertz, it means that the CPU will ask “What next?” three billion times
per second. You are going to have to learn how to talk fast to keep up with
the CPU.

(2) Main Memory, 
 used to store information that the CPU needs in a
hurry. The main memory is nearly as fast as the CPU. But the information
stored in the main memory vanishes when the computer is turned off.

(3)Secondary Memory,
used to store information, but it is much
slower than the main memory. The advantage of the secondary memory is
that it can store information even when there is no power to the computer.
Examples of secondary memory are disk drives or flash memory (typically
found in USB sticks and portable music players).

(4) Input Device, and 
are used to insert data into the pc e.g keyboard

(5) Output Device. For example, 
used to display and produce the information e.g speaker, monitor.

“What is the human equivalent to a Central Processing Unit”?
the human brain

Exercise 9: How do you fix a “Syntax Error”?
check spellings are correct.
